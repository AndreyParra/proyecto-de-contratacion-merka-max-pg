<div class="col-md-6 col-xs-12">
      
  <div class="box box-success">
       <div class="box box-solid bg-green-gradient">
         
         <div class="box-header">
          
           <i class="fa fa-th"></i>

           <h3 class="box-title">Aspirantes registrados por cargo</h3>

         </div>


         <div class="box-body border-radius-none nuevoGraficoEmpleado">

           <div class="chart" id="line-chart" style="height: 250px;"></div>

         </div>

       </div>

</div>
       
</div>

<script>
  
  var line = new Morris.Line({
     element          : 'line-chart',
     resize           : true,
     data             : [
       { y: '2011 Q1', item1: 2666 },
       { y: '2011 Q2', item1: 2778 },
       { y: '2011 Q3', item1: 4912 },
       { y: '2011 Q4', item1: 3767 },
       { y: '2012 Q1', item1: 6810 },
       { y: '2012 Q2', item1: 5670 },
       { y: '2012 Q3', item1: 4820 },
       { y: '2012 Q4', item1: 15073 },
       { y: '2013 Q1', item1: 10687 },
       { y: '2013 Q2', item1: 8432 }
     ],
     xkey             : 'y',
     ykeys            : ['item1'],
     labels           : ['Item 1'],
     lineColors       : ['#efefef'],
     lineWidth        : 2,
     hideHover        : 'auto',
     gridTextColor    : '#fff',
     gridStrokeWidth  : 0.4,
     pointSize        : 4,
     pointStrokeColors: ['#efefef'],
     gridLineColor    : '#efefef',
     gridTextFamily   : 'Open Sans',
     gridTextSize     : 10
   });


  $('.knob').knob();

   // jvectormap data
   var visitorsData = {
     US: 398, // USA
     SA: 400, // Saudi Arabia
     CA: 1000, // Canada
     DE: 500, // Germany
     FR: 760, // France
     CN: 300, // China
     AU: 700, // Australia
     BR: 600, // Brazil
     IN: 800, // India
     GB: 320, // Great Britain
     RU: 3000 // Russia
   };

  
</script>
