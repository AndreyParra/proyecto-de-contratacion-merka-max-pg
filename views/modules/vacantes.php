<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Vacantes <i class="icon-documents"></i></h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Administrar Vacantes</li>
    </ol>
  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
        <button class="medida btn btn-success pull-right" data-toggle="modal" data-target="#modalVacante"> Vacante <i class="fas fa-plus"></i></button>
      </div>
      <div class="box-body">  
        <div class="row">
          <?php 
          $TraerVacante = VacanteController::MostrarVacantes();


            foreach ($TraerVacante as $key => $value) {   

            if ($value["Type"] == "Cajero") {

              echo '

              <div class="col-lg-3 col-xs-6">

                <div class="small-box cajaAmarilla borde">
                  <div class="inner">
                    <h2><b>'.$value["Type"].'</b></h2>

                    <label>Salario: '.$value["Wage"].' $</label>
                    <br>

                    <em><b>Descripción:</b> '.$value["Description"].' </em>
                  </div>
                  <div class="icon">
                    <i class="icon-shopping-cart1"></i>
                  </div>
                  <a href="#" class="small-box-footer btnMostrarVacante" data-toggle="modal"data-target="#EditarVacante" idVacante="'.$value["ID"].'">Editar <i class="icon-edit-3"></i></a>
                </div>
              </div>';
              
             }else if ($value["Type"] == "Auxiliar") {

              echo '

              <div class="col-lg-3 col-xs-6">

                <div class="small-box cajaRoja borde">
                  <div class="inner">
                    <h2><b>'.$value["Type"].'</b></h2>

                    <label>Salario: '.$value["Wage"].' $</label>
                    <br>

                    <em><b>Descripción:</b> '.$value["Description"].' </em>
                  </div>
                  <div class="icon">
                    <i class="icon-user-plus"></i>
                  </div>
                  <a href="#" class="small-box-footer btnMostrarVacante" data-toggle="modal"data-target="#EditarVacante" idVacante="'.$value["ID"].'">Editar <i class="icon-edit-3"></i></a>
                </div>
              </div>';
              
             }else  if ($value["Type"] == "Domiciliario") {

              echo '

              <div class="col-lg-3 col-xs-6">

                <div class="small-box cajaVerde borde">
                  <div class="inner">
                    <h2><b>'.$value["Type"].'</b></h2>

                    <label>Salario: '.$value["Wage"].' $</label>
                    <br>

                    <em><b>Descripción:</b> '.$value["Description"].' </em>
                  </div>
                  <div class="icon">
                    <i class="icon-map-pin"></i>
                  </div>
                  <a href="#" class="small-box-footer btnMostrarVacante" data-toggle="modal"data-target="#EditarVacante" idVacante="'.$value["ID"].'">Editar <i class="icon-edit-3"></i></a>
                </div>
              </div>';
              
             } else  if ($value["Type"] == "Bodeguista") {

              echo '

              <div class="col-lg-3 col-xs-6">

                <div class="small-box cajaPurpura borde">
                  <div class="inner">
                    <h2><b>'.$value["Type"].'</b></h2>

                    <label>Salario: '.$value["Wage"].' $</label>

                    <br>

                    <em><b>Descripción:</b> '.$value["Description"].' </em>
                  </div>
                  <div class="icon">
                    <i class="icon-box"></i>
                  </div>
                  <a href="#" class="small-box-footer btnMostrarVacante" data-toggle="modal"data-target="#EditarVacante" idVacante="'.$value["ID"].'">Editar <i class="icon-edit-3"></i></a>
                </div>
              </div>';
              
             }

            }
           ?>

           

        </div>
      </div>
    </div>
  </section>
</div>
<div>  
</div>


<!-- EDITAR VACANTE -->

<div id="EditarVacante" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" tabindex="" aria-hidden="true">
  <form method="post" enctype="multipart/form-data" id="formulario">
    <div class="modal-dialog ">
      <div class="modal-content " style=" border-radius: 15px;">

        <div class="modal-header box box-danger" style="background: rgba(215,57,37,1); color: rgba(255, 255, 255, .7);">
          <button type="button" class="close" data-dismiss="modal" style="color:white;">&times;</button>
          <h4 class="modal-title">Editar Vacante <i class="icon-edit"></i></h4>
        </div>

        <div class="modal-body">
          <div class="container-fluid text-left">
              <div class="box-body">
               <div class="form-group">
                 <label for=Name>Vacante</label>
                 <select name="editarName" class="color medida form-control" id="Name">
                  
                  <option value="" id="editarNameOcu1"></option>
                  <option value="" id="editarNameOcu2"></option>
                  <option value="" id="editarNameOcu3"></option>
                  <option value="" id="editarNameOcu4"></option>

                 </select>
               </div>
               <div class="form-group">
                 <label for=Wage>Salario</label>
                 <input type="number" class="color medida form-control" id="editarWage" name="editarWage"  value="" >
               </div>
               <div class="form-group">
                 <label for="Description">Descripción</label>
                 <input type="text" class="color medida form-control" id="editarDescription" name="editarDescription" value="" >
               </div>
             </div>
             <div class="modal-footer">
               <div class="btn-group pull-right">
                 <button type="reset" class="medida btn btn-default">Limpiar <i class="icon-delete1"></i></button>

                  <button type="submit" class="medida btn btn-success">Guardar <i class="icon-save2"></i></button>
               </div>
             </div>
          </div>
        </div>
      </div>
    </div>
    <?php 

      $EditarVacante = new VacanteController();  
      $EditarVacante -> EditarVacante();      

     ?>
</form>
</div>

<!-- REGISTRAR VACANTE -->

<div id="modalVacante" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" tabindex="" aria-hidden="true">
  <form method="post" enctype="multipart/form-data" class="validarFormulario">
    <div class="modal-dialog ">
     <div class="modal-content " style=" border-radius: 15px;">
       
       <div class="modal-header box box-success" style="background: rgb(0,141,76); color: rgba(255, 255, 255, .7); ">
         <button type="button" class="close" data-dismiss="modal" style="color:white;">&times;</button>
         <h4 class="modal-title">Nueva Vacante <i class="icon-plus"></i></h4>
       </div>
        
        <div class="modal-body">
              <div class="box-body">
               <div class="form-group validar">
                 <label for="Name">Vacante</label>
                 <select name="Name" class="color medida form-control" id="Name">
                  <option value="">Seleccione...</option>
                  <?php 
                    $Vacantes = VacanteController::Opciones();

                    foreach ($Vacantes as $key => $value) {   
                    echo '<option value="'.$value["ID"].'">'.$value["Type"].'</option>';

                    }
                   ?>
                 </select>
               </div>
               <div class="form-group validar">
                 <label for= "Wage">Salario</label>
                 <input type="number" class="color medida form-control" id="Wage" name="Wage" placeholder="Digíte el salario de la Vacante">
               </div>
               <div class="form-group validar">
                 <label for="Description">Descripción</label>
                 <textarea class="color medida form-control" id="Description" name="Description" placeholder="Digíte la descripción de la vacante"></textarea>
               </div>
             </div>
             <div class="modal-footer">
               <div class="btn-group pull-right">
                 <button type="reset" class="medida btn btn-default">Limpiar <i class="icon-delete1"></i></button>

                  <button type="submit" class="medida btn btn-success">Guardar <i class="icon-save2"></i></button>
               </div>
             </div>
        </div>
      </div>
    </div>
  </div>
  <?php 

             $nuevaVacante = new VacanteController();
             $nuevaVacante -> CrearVacante();

   ?>
</form>
</div>





<script src="views/assets/js/templete.js"></script>


