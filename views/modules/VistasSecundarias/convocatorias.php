<div class="content-wrapper"><br><br>   
  <section>

    <div class="caja"> 
      <div class="container"> 
        <h2 class="tit">Trabaja Con Nosotros</h2>
        <h4 class="text-muted t ">Diligencia Tu Hoja De Vida</h4>
      </div>
      <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'Vacan')" id="defaultOpen">  <i class="icon-documents"></i> Vacantes</button>
        <button class="tablinks" onclick="openCity(event, 'Info')"> <i class="fa fa-user-plus"></i> Info-Personal </button>
        <button class="tablinks" onclick="openCity(event, 'Educ')"> <i class="fa fa-graduation-cap"></i> Educación</button>
        <button class="tablinks" onclick="openCity(event, 'Exper')"> <i class="fa fa-address-book"></i> Experiencia</button>
        <button class="tablinks" onclick="openCity(event, 'Refer')"><i class="fa fa-users"></i> Referencias</button>
        <button class="tablinks" onclick="openCity(event, 'Idio')"><i class="fa fa-language"></i> Idiomas</button>
      </div>
      <div id="Vacan" class="tabcontent">
       <div class="row">
          <?php 
            $TraerVacante = VacanteController::MostrarVacantes();
            foreach ($TraerVacante as $key => $value) {   
              if ($value["Type"] == "Cajero") {
                echo '
                <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-orange">
                <div class="inner">
                <h2><b>'.$value["Type"].'</b></h2>
                <label>Salario: '.$value["Wage"].' $</label>
                <br>
                <em><b>Descripción:</b> '.$value["Description"].' </em>
                </div>
                <div class="icon">
                <i class="icon-shopping-cart1"></i>
                </div>
                <a href="#" class="small-box-footer">
                Aplicar <i class="fa fa-arrow-circle-right"></i>
                </a>
                </div>
                </div>';
              }else if ($value["Type"] == "Auxiliar") {
                echo
                 '
                <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red">
                <div class="inner">
                <h2><b>'.$value["Type"].'</b></h2>
                <label>Salario: '.$value["Wage"].' $</label>
                <br>
                <em><b>Descripción:</b> '.$value["Description"].' </em>
                </div>
                <div class="icon">
                <i class="icon-user-plus"></i>
                </div>
                <a href="#" class="small-box-footer">
                Aplicar <i class="fa fa-arrow-circle-right"></i>
                </a>
                </div>
                </div>';
              }else  if ($value["Type"] == "Domiciliario") {
                echo '
                <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                <div class="inner">
                <h2><b>'.$value["Type"].'</b></h2>
                <label>Salario: '.$value["Wage"].' $</label>
                <br>
                <em><b>Descripción:</b> '.$value["Description"].' </em>
                </div>
                <div class="icon">
                <i class="icon-map-pin"></i>
                </div>
                <a href="#" class="small-box-footer">
                Aplicar <i class="fa fa-arrow-circle-right"></i>
                </a>                </div>
                </div>';
              } else  if ($value["Type"] == "Bodeguista") {
                echo '
                <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-purple">
                <div class="inner">
                <h2><b>'.$value["Type"].'</b></h2>
                <label>Salario: '.$value["Wage"].' $</label>
                <br>
                <em><b>Descripción:</b> '.$value["Description"].' </em>
                </div>
                <div class="icon">
                <i class="icon-box"></i>
                </div>
                <a href="#" class="small-box-footer">
                Aplicar <i class="fa fa-arrow-circle-right"></i>
                </a>
                </div>
                </div>';
              }
            }
          ?>
        </div>
      </div>
      <div id="Info" class="tabcontent">
        <div class="row">
          <div class="col-sm-3">
            <div class="form-group">
             <label for="Name" >Nombre(s)<span>*</span></label>
              <div class="validar">
                <div class="input-group"> 
                <input required type="text" class="color form-control" name="Name" placeholder="Nombre">
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label for="LastName" >Apellidos<span>*</span></label>
              <div class="validar">
                <div class="input-group"> 
                    <input required type="text" class=" color form-control" name="LastName" placeholder="Apellido">
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
            <label for="TDocument" >Tipo De Documento<span>*</span></label>
              <div class="validar">
                <div class="input-group"> 
                  <select name="TDocument" id="TDocument" class=" color form-control"> 
                    <option value="">Seleccione...</option>
                    <option value="CC">Cédula Ciudadania</option>
                    <option value="CE">Cédula Extranjera</option>
                    <option value="PA">Pasaporte</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label for="NumDocument"># Documento<span>*</span></label>
              <div class="validar">
                <div class="input-group"> 
                  <input required type="number" class=" color form-control " name="NumDocument" id="NumDocument" placeholder="Documento">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row"> 
          <div class="col-sm-3">
            <div class="form-group">
              <label for="Phone" >Teléfono</label>
              <div class="validar">
                <div class="input-group"> 
                  <input required type="number" class=" color form-control " name="CPhone" id="CPhone" placeholder="Teléfono">
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label for="CellPhone">Celular<span>*</span> </label>
              <div class="validar">
                <div class="input-group"> 
                  <input required type="text" class=" color form-control" name="CCellphone" id="CCellphone" placeholder="Número de Movil">
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label for="Address" >Dirección<span>*</span></label>
              <div class="validar">
                <div class="input-group"> 
                  <input required  type="text" class=" color form-control " name="CAddress" id="CAddress" placeholder="Dirección">
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label for="Mail" >Correo electrónico<span>*</span></label>
              <div class="validar">
                <div class="input-group"> 
                  <input required type="text" class=" color form-control " name="CMail" id="CMail" placeholder="Email">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3">
            <div class="form-group">
             <label for="Gender" >Género<span>*</span></label>
              <div class="validar">
                <div class="input-group">
                  <select name="CGender" id="CGender" class=" color form-control"> 
                    <option value="">Seleccione...</option>
                    <option value="F">Femenino</option>
                    <option value="M">Masculino</option>
                    <option value="O">Prefiero No Decir</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label for="Maritalstatus">Estado Civil<span>*</span></label>
              <div class="validar">
                <div class="input-group"> 
                  <select name="CMaritalstatus" id="CMaritalstatus" class="color form-control">  <option value="">Seleccione...</option>
                    <option value="S">Soltero</option>
                    <option value="C">Casado</option>
                    <option value="D">Divordiado</option>
                    <option value="V">Viudo</option>
                    <option value="U">Union Libre</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label for="BirthDate" >Fecha De Nacimiento<span>*</span></label>
              <div class="validar">
                <div class="input-group"> 
                  <input required type="date" class=" color form-control" id="CBirthDate" name="CBirthDate" placeholder="Fecha De Nacimiento">
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
            <label for="Description">Descripción<span>*</span> </label>
              <div class="validar">
                <div class="input-group"> 
                  <textarea class=" color form-control" name="CDescription" id="CDescription">
                  </textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row"> 
          <div class="col-sm-6">
            <div class="form-group">
            <label for="Photo">Foto<span>*</span></label>
              <div class="input-group"> 
                <input type="file" class="newPhoto" name="newPhoto">
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <img src="views/assets/img/avatar.png" class="img-thumbnail previsualizar" width="70px">
              <p class="help-block">Peso máximo de 200 MB</p>
            </div>
          </div>
        </div>
      </div>
      <div id="Educ" class="tabcontent">
        <h3> Educación </h3>
          <div class="row clearfix">
            <div class="col-md-12 table-responsive">
              <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                <thead>
                  <tr >
                    <th class="text-center">
                       <label for="Title">Título<span>*</span></label>
                    </th>
                    <th class="text-center">
                       <label for="NameCareer" class="text-lg">Carrera<span>*</span></label>
                    </th>
                    <th class="text-center">
                       <label for="Time" class="text-lg">Tiempo<span>*</span></label>
                    </th>
                    <th class="text-center">
                       <label for="Certification" class="text-lg">Certificación<span>*</span></label>
                    </th>
                    <th>  
                      <a id="agregar" class="btn btn-primary float-right"><i class="fa fa-plus">  </i></a>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr id='addr0' data-id="0" class="hidden">
                    <td data-name="title">
                      <div class="validar">
                        <select name="Level" id="Level" class=" color form-control"> 
                          <option value="">
                            Seleccione...
                          </option>
                          <?php   
                          $TraerLevel = AspiranteController::MostrarLevel();
                          foreach ($TraerLevel as $key => $value) { 
                            ?> 
                            <option value=<?php echo '"'.$value["ID"].'"'; ?>><?php echo $value["Name"]; ?>
                          </option>
                          <?php 
                            } 
                          ?>
                        </select>      
                      </div>
                    </td>
                    <td data-name="institution">
                      <div class="validar">
                        <input required type="text" class=" color form-control " name="NameCareer" id="NameCareer" placeholder="Institución">
                      </div>
                    </td>
                    <td data-name="time">
                      <div class="input-group"> 
                        <input required type="text" class=" color form-control " name="ETime" id="ETime"placeholder="Años Estudiados">
                      </div>
                    </td>
                    <td data-name="certificate">
                      <div class="input-group"> 
                        <input required type="file" class="Certificacion" name="Certificacion">
                      </div>
                    </td>
                    <td data-name="del">
                    <button name="del0" class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
      </div>
      <div id="Exper" class="tabcontent">
        <h3> Experiencia </h3>
        <div class="row clearfix">
          <div class="col-md-12 table-responsive">
            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
              <thead>
                <tr >
                  <th class="text-center">
                   <label for="Company" class="text-lg">Compañia<span>*</span></label>
                  </th>
                  <th class="text-center">
                   <label for="Boss" class="text-lg">Jefe Inmediato<span>*</span></label>
                  </th>
                  <th class="text-center">
                   <label for="DocBoss" class="text-lg">Documento Del Jefe<span>*</span></label>
                  </th>
                  <th class="text-center">
                   <label for="Phone" class="text-lg">Teléfono</label>
                  </th>
                  <th class="text-center">
                   <label for="Role" class="text-lg">Cargo Que Ejercia<span>*</span></label>
                  </th>
                  <th class="text-center">
                   <label for="Time" class="text-lg">Tiempo<span>*</span></label>
                  </th>
                  <th>  
                  <a id="AgregarExperiencia" class="btn btn-primary float-right"><i class="fa fa-plus">  </i></a>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr id='addr0' data-id="0" class="hidden">
                  <td data-name="Company">
                    <div class="validar">
                        <input required type="text" class=" color form-control" name="Company" id="Company" placeholder="Compañia">
                    </div>
                  </td>
                  <td data-name="Boss">
                    <div class="validar">
                      <input required type="text" class=" color form-control " name="Boss" id="Boss" placeholder="Jefe Anterior">
                    </div>
                  </td>
                  <td data-name="DocBoss">
                    <div class="input-group"> 
                      <input required type="Number" class=" color form-control " name="DocBoss" id="DocBoss" placeholder="Documento">
                    </div>
                  </td>
                  <td data-name="BossPhone">
                    <div class="input-group"> 
                       <input required type="text" class=" color form-control" name="BossPhone" id="BossPhone" placeholder="Número de Teléfono">
                    </div>
                  </td>
                  <td data-name="Role">
                    <div class="input-group"> 
                      <input required type="text" class=" color form-control " name="Role" id="Role" placeholder="Cargo">            
                    </div>
                  </td>
                  <td data-name="WTime">
                    <div class="input-group"> 
                      <input required type="text" class=" color form-control" name="WTime" id="WTime" placeholder="Años Estudiados">            
                    </div>
                  </td>
                  <td data-name="del">
                    <button name="del0" class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div id="Refer" class="tabcontent">
        <div class="row clearfix">
          <div class="col-md-12 table-responsive">
            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
              <thead>
                <tr >
                  <th class="text-center">
                   <label for="NumDocument" class="text-lg"># Documento<span>*</span></label>
                  </th>
                  <th class="text-center">
                   <label for="Name">Nombre(s)<span>*</span></label>
                  </th>
                  <th class="text-center">
                   <label for="Occupation">Ocupación<span>*</span></label>
                  </th>
                  <th class="text-center">
                   <label for="Association">Parentesco<span>*</span></label>
                  </th>
                  <th class="text-center">
                   <label for="Phone" class="text-lg">Teléfono<span>*</span></label>
                  </th>
                  <th>  
                  <a id="AgregarExperiencia" class="btn btn-primary float-right"><i class="fa fa-plus">  </i></a>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr id='addr0' data-id="0" class="hidden">
                  <td data-name="RNumDocument">
                    <div class="validar">
                       <input required type="number" class=" color form-control "name="RNumDocument" id="RNumDocument" placeholder="Número de documento">
                    </div>
                  </td>
                  <td data-name="RName">
                    <div class="validar">
                       <input required type="text" class=" color form-control " id="RName" name="RName" placeholder="Nombre"></span>
                    </div>
                  </td>
                  <td data-name="Occupation">
                    <div class="input-group"> 
                        <input required type="text" class=" color form-control " id="Occupation" name="Occupation" placeholder="Ocupación">
                    </div>
                  </td>
                  <td data-name="Association">
                    <div class="input-group"> 
                         <input required type="text" class=" color form-control " id="Association" name="Association" placeholder="Parentesco">
                    </div>
                  </td>
                  <td data-name="RPhone">
                    <div class="input-group"> 
                     <input required type="text" class=" color form-control" name="RPhone" id="RPhone" placeholder="Número de Teléfono">           
                    </div>
                  </td>
                  <td data-name="del">
                    <button name="del0" class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        
      </div>
      <div id="Idio" class="tabcontent">
        <h3> Idioma </h3>
          <div class="row clearfix">
            <div class="col-md-12 table-responsive">
              <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                <thead>
                  <tr >
                    <th class="text-center">
                      <label for="Language" class="text-lg">Idioma<span>*</span></label>
                    </th>
                    <th class="text-center">
                      <label for="Institution" class="text">Institución<span>*</span></label>
                    </th>
                    <th class="text-center">
                      <label for="Certificacion">Certificación<span>*</span></label>
                    </th>
                    <th>  
                      <a id="agregar" class="btn btn-primary float-right"><i class="fa fa-plus">  </i></a>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr id='addr0' data-id="0" class="hidden">
                    <td data-name="languange">
                      <div class="validar">
                        <select name="Language" id="Language" class=" color form-control"> 
                          <option value="">Seleccione...</option>
                          <?php   
                          $TraerLanguage = AspiranteController::MostrarLanguage();
                          foreach ($TraerLanguage as $key => $value) { ?> 
                              <option value=<?php echo '"'.$value["ID"].'"'; ?>><?php echo $value["Name"];?>
                              </option>
                          <?php 
                          
                              } 
                          ?>
                        </select>
                      </div>
                    </td>
                    <td data-name="institution">
                      <div class="validar">
                        <input required type="text" class=" color form-control " name="LInstitution" id="LInstitution" placeholder="Institución">
                      </div>
                    </td>
                    <td data-name="certificate">
                      <div class="input-group"> 
                        <input required type="file" class="Certificacion" name="Certificacion">
                      </div>
                    </td>
                    <td data-name="del">
                    <button name="del0" class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <p>
      
    </p>
  </section>
</div>
<script>
  function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }

  document.getElementById("defaultOpen").click();

  $(document).ready(function() {
    $("#agregar").on("click", function() {
      
      var newid = 0;
      $.each($("#tab_logic tr"), function() {
        if (parseInt($(this).data("id")) > newid) {
          newid = parseInt($(this).data("id"));
        }
      });
      newid++;
      
      var tr = $("<tr></tr>", {
        id: "addr"+newid,
        "data-id": newid
      });
      
      $.each($("#tab_logic tbody tr:nth(0) td"), function() {
        var td;
        var cur_td = $(this);

        var children = cur_td.children();

          if ($(this).data("name") !== undefined) {
            td = $("<td></td>", {
              "data-name": $(cur_td).data("name")
            });

            var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
            c.attr("name", $(cur_td).data("name") + newid);
            c.appendTo($(td));
            td.appendTo($(tr));
          } else {
            td = $("<td></td>", {
              'text': $('#tab_logic tr').length
            }).appendTo($(tr));
          }
        });
      
      $(tr).appendTo($('#tab_logic'));
      
      $(tr).find("td button.row-remove").on("click", function() {
       $(this).closest("tr").remove();
     });
    });

  var fixHelperModified = function(e, tr) {
    var $originals = tr.children();
    var $helper = tr.clone();

    $helper.children().each(function(index) {
      $(this).width($originals.eq(index).width())
    });
    return $helper;
  };
  $(".table-sortable tbody").sortable({
    helper: fixHelperModified      
  }).disableSelection();

  $(".table-sortable thead").disableSelection();

  $("#agregar").trigger("click");
  });
</script>
<script src="views/assets/js/templete.js"></script>
