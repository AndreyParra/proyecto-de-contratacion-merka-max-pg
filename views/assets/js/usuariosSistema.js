  $('.listar').on("click", "#my-select", function() { 
      
      var idUsuario = $('#my-select').val(); 

      if(idUsuario != "sinValor") {

        $('#labelCorreo').removeClass('text-danger');
        $('#labelCorreo').addClass('text-success');

        $('#labelCorreo').html("La contraseña será enviada al siguiente correo electrónico:");

      }else {

        $('#labelCorreo').removeClass('text-success');
        $('#labelCorreo').addClass('text-danger');
     
            $('#labelCorreo').html("¡Seleccione el usuario a crear!");

      }


        var datos = new FormData();
          datos.append("idUsuario", idUsuario);

      $.ajax({

        url:"ajax/empleados.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta){

               
          $('#correo').val(respuesta["Mail"]);

        }

      });
  }); 
