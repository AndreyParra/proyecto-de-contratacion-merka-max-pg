<?php 

  class UsuarioController {

  	
    public static function ctrContarUsuarios() {

      $valor = "A";
  	  
  	  $respuesta = Usuario::contarUsuarios($valor);
  	  
  	  return $respuesta;
  	}

    
    public static function ctrGraficarRoles() {

      $respuesta = Usuario::graficarRoles();

      return $respuesta;

    }

    
    public static function ctrChatUsuario() {

      $respuesta = Usuario::verMensajes();

      return $respuesta;
    }
   

    public static function ctrListarRoles() {

      $valor = 'A';

      $respuesta = Usuario::listarRoles($valor);

      return $respuesta;
    }

    
    public static function ctrNuevoMensaje() {

      if (isset($_POST["msg"])) {

        $mensaje = $_POST["msg"];

        $respuesta = Usuario::nuevoMensaje($mensaje);

      }
    }

   
    public static function ctrBuscarUsuario() {

      $respuesta = Usuario::buscarUsuario();

      return $respuesta;
    }
    

    public static function ctrNuevoUsuario() {

        if(isset($_POST["datosUsuario"])) {

          if($_POST["datosUsuario"] != "sinValor") {

            $idUsuario = $_POST["datosUsuario"];



            //Generar contraseña
            $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            $password = "";

            for($i=0; $i<=9; $i++) {
               
               //obtenemos un caracter aleatorio escogido de la cadena de caracteres
               $password .= substr($str,rand(0,62),1);     
            }

            $email = $_POST["correo"];

            $para = $email . ', ';
            $para .= 'aliadosurtimaxmerkamax@gmail.com';

            $titulo = 'Asignación de contraseña del sistema de contratación Merka Max PG';

            $mensaje = '


            <html>
            <head>

              <title>Asignación de nueva contraseña</title>

            </head>

            <body>
              
              <h3>Cordial saludo.</h3>

              <p>Le damos la bienvenida al sistema de contratación de Merka Max PG.</p>
              <br>
              <p>Su nueva contraseña es: '.$password.'. Le recomendamos que la cambie dando clic en el botón de configuración.</p>
              <br>
              <hr>
              <p> POR FAVOR NO RESPONDER ESTA MENSAJE</p>
              <hr>
              <br>
              <p><b>ALIADO SURTIMAX | MERKA MAX PG</b><br>
              Bogotá, Cundinamarca <br>
              Cll 166 Bis #54-93 <br></p><br>

              <img src="https://i.postimg.cc/wMdPB2ZW/logo-aliado.png">


            </body>

            </html>';

            $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
            $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            $cabeceras .= 'From: <aliadosurtimaxmerkamax@gmail.com>' . "\r\n";

            $envio = mail($para, $titulo, $mensaje, $cabeceras );





            $encriptar = crypt($password, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

            $respuesta = Usuario::nuevoUsuario( $encriptar, $idUsuario);

            $respuesta1 = Empleado::editarPerfil($idUsuario);


            if($respuesta == "ok" && $respuesta1 == "ok") {

              echo "<script>

                 Swal.fire({
                   icon: 'success',
                   title: '¡Usuario registrado correctamente!',
                 })


              </script>";

            }else {

              echo "<script>

                 Swal.fire({
                   icon: 'error',
                   title: '¡Usuario ya registrado!',
                 })


              </script>";

            }

          }else {
            
            echo "<script>

               Swal.fire({
                 icon: 'error',
                 title: '¡Debe seleccionar un empleado existente!',
               })


            </script>";
          }


        }
      }

  

    public static function ctrAgregarNota() {

      if (isset($_POST["item"])) {


        $nombre = $_SESSION["nombre"];

        $apellido = $_SESSION["apellido"];

        $valor = $_POST["item"];

        $respuesta = Usuario::agregarNota($nombre, $apellido, $valor);
        

      }
    }



    public static function ctrVerNotas() {

      $respuesta = Usuario::verNotas();

      return $respuesta;

    }



    public static function ctrEditarNota() {

      if (isset($_POST["editarItem"])) {


        $nombre = $_SESSION["nombre"];

        $apellido = $_SESSION["apellido"];
        
        $editarItem = $_POST["editarItem"];

        $listarItem = $_POST["listarItem"];


        $respuesta = Usuario::editarNota($nombre, $apellido, $editarItem, $listarItem);


      }
    }

    public function ctrEliminarNotas() {

      if(isset($_POST["eliminarItem"])){


        $valor = $_POST["eliminarItem"];

        $respuesta = Usuario::eliminarNotas($valor);
      }
    }

    public static function ctrEditarClave() {

      if(isset($_POST["conActual"])) {

       
        $conActual = crypt($_POST["conActual"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
       
        $conNueva = crypt($_POST["conNueva"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
        
        $codigo =  $_SESSION["codigo"];

        $respuesta = Usuario::editarClave($conActual, $conNueva, $codigo);


        if($respuesta == "ok") {

          echo "<script>

             Swal.fire({
               icon: 'success',
               title: 'Su contraseña fue actualizada con éxito',
             })


          </script>";

        }else if($respuesta == "false") {

          echo "<script>

             Swal.fire({
               icon: 'error',
               title: 'Oops...',
               title: 'Verifique sus datos e intente de nuevo',
             })


          </script>";

        }

      }
    }

    public static function ctrPreguntarClave($clave, $codigo) {


      $valor = crypt($clave, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

      $respuesta = Usuario::preguntarClave($valor, $codigo);

      return $respuesta;

    }

    public static function ctrTotalLogin(){

      $valor = 'A';

      $respuesta = Usuario::totalLogin($valor);

      return $respuesta;

    } 


    
  	
  }