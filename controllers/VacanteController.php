<?php

  class VacanteController {
  	
  	public static function ctrContarVacantes() {
     
     $tabla = "vacant";

     $respuesta = Vacante::contarVacantes($tabla);

     return $respuesta;

     
  	}

  	  	static public function CrearVacante() {
  	      
  	  		if (isset($_POST["Name"])) {

  	  	  	$tabla = "vacant";
  	        
  	        $Name = $_POST["Name"];
  	        
  	        $Wage = $_POST["Wage"];
  	        
  	        $Description = $_POST["Description"];
  	        
  	        $ID = $_SESSION["codigo"];

  	  			           $resultado = Vacante::Insertar($tabla, $Name, $Wage, $Description, $ID);

  	           
  	  			    if ($resultado == "true") {
  	  			    	
  	  			    	echo "<script>

  	  			    	   Swal.fire({
  	  			    	     icon: 'success',
  	  			    	     title: '¡Operación completada!',
  	  			    	     title: 'Datos almacenados correctamente',
  	  			    	   })


  	  			    	</script>";
  	  			    }	
  	        else {

  	  				echo "<script>

  	  				   Swal.fire({
  	  				     icon: 'error',
  	  				     title: 'Oops...',
  	  				     title: '¡Revisa el formato de los campos e intenta de nuevo!',
  	  				   })


  	  				</script>";
  	  			}
  	  		}
  	}


  	static public  function EditarVacante() {
  	      
  	  if (isset($_POST["editarName"])) {

  	        $tabla = "vacant";
  	       
            $Name = $_POST["editarName"];

  	        $Wage = $_POST["editarWage"];

  	        $Description = $_POST["editarDescription"];

            $ID_employee = $_SESSION["codigo"];


  	        $resultado = Vacante::Update($tabla, $Name, $Wage, $Description, $ID_employee,);

  	            if ($resultado == "true") {
  	              
  	              echo "<script>

  	                 Swal.fire({
  	                   icon: 'success',
  	                   title: '¡Operación completada!',
  	                   title: 'Datos actualizados correctamente',
  	                 })


  	              </script>";
  	            } 
  	        else {

  	          echo "<script>

  	             Swal.fire({
  	               icon: 'error',
  	               title: 'Oops...',
  	               title: '¡Revisa el formato de los campos e intenta de nuevo!',
  	             })


  	          </script>";
  	        }
  	      }
  	}

  	  	static public  function MostrarVacantes() {

  	      $tabla = "vacant";
  	      $tabla1 = "occupation";
  	      
  	      $respuesta = Vacante::Select($tabla, $tabla1);
  	      
  	      return $respuesta;
  	    }

  	    static public  function DatosVacante($item, $valor) {

  	      $tabla = "vacant";

  	      $respuesta = Vacante::InfoVacante($tabla, $item, $valor);
  	      
  	      return $respuesta;
  	    }

  	    static public function Opciones(){
  	      $tabla = "occupation";
  	       $respuesta = Vacante::Roles($tabla);
  	       return $respuesta;
  	    }

  	    static public function MostrarLevel(){
  	      $tabla = "level";
  	       $respuesta = Vacante::Datos($tabla);
  	       return $respuesta;
  	    }

  	    static public function MostrarLanguage(){
  	      $tabla = "typelanguage";
  	       $respuesta = Vacante::Datos($tabla);
  	       return $respuesta;
  	    } 
  }
