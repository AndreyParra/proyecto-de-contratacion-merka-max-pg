<?php

  require_once "../controllers/VacanteController.php";
  require_once "../models/Vacante.php";

  class ajaxVacante {

  	//Editar vacante

  	public $idVacante;

  	public function ajaxMostrarVacantes() {

  		$tabla = "vacant";

  		$item = "ID";
  		
  		$valor = $this->idVacante;

  		$respuesta = Vacante::InfoVacante($tabla, $item, $valor);

  		echo json_encode($respuesta);

  	}
  }



  /*=============================================
  EDITAR VACANTE
  =============================================*/
  if(isset($_POST["idVacante"])){

    $editar = new ajaxVacante();
    $editar -> idVacante = $_POST["idVacante"];
    $editar -> ajaxMostrarVacantes();

  }