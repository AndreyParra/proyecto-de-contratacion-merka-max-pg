<?php

require_once "Conexion.php";

    class Cita extends Conexion {

      static public function nuevaCita($fechaInicio, $fechaFinal, $fecha, $horaIni, $horaFin, $comentarios) {

          $stmt = Conexion::conectar()->prepare("INSERT INTO interview (date_start, date_end, date, hour_start, hour_end, comments, Id_Asp) VALUES( :date_start, :date_end, :date, :hour_start, :hour_end, :comments, 0)");


          $stmt-> bindParam(":date_start", $fechaInicio, PDO::PARAM_STR);
          $stmt-> bindParam(":date_end", $fechaFinal, PDO::PARAM_STR);
          $stmt-> bindParam(":date", $fecha, PDO::PARAM_STR);
          $stmt-> bindParam(":hour_start", $horaIni, PDO::PARAM_STR);
          $stmt-> bindParam(":hour_end", $horaFin, PDO::PARAM_STR);
          $stmt-> bindParam(":comments", $comentarios, PDO::PARAM_STR);

          if($stmt->execute()){
            
            return "ok";
          
          } else{
            
            return "false";
          }
          


          $stmt->close();

          $stmt = null;

        }

        static public function editarCita($fechaInicio, $fechaFinal, $fecha, $horaIni, $horaFin, $comentarios, $id) {

            $stmt = Conexion::conectar()->prepare("UPDATE interview SET date_start = :date_start, date_end = :date_end, date = :date, hour_start = :hour_start, hour_end = :hour_end, comments = :comments WHERE id = :id");


            $stmt-> bindParam(":date_start", $fechaInicio, PDO::PARAM_STR);
            $stmt-> bindParam(":date_end", $fechaFinal, PDO::PARAM_STR);
            $stmt-> bindParam(":date", $fecha, PDO::PARAM_STR);
            $stmt-> bindParam(":hour_start", $horaIni, PDO::PARAM_STR);
            $stmt-> bindParam(":hour_end", $horaFin, PDO::PARAM_STR);
            $stmt-> bindParam(":comments", $comentarios, PDO::PARAM_STR);
            $stmt-> bindParam(":id", $id, PDO::PARAM_INT);

            if($stmt->execute()){
              
              return "ok";
            
            } else{
              
              return "false";
            }
            
            $stmt->close();

            $stmt = null;

          }


       public static function listarCita($item, $valor) {


        if ($item == "Id") {
            
              $stmt = Conexion::conectar()->prepare("SELECT * FROM interview WHERE id = :valor");

              $stmt-> bindParam(":valor", $valor, PDO::PARAM_STR);

              $stmt-> execute();

              return $stmt-> fetch();
            
          
        }else{
                  
              $stmt = Conexion::conectar()->prepare("SELECT * FROM interview");      

              $stmt-> execute();

              return $stmt-> fetchAll();    
        }
        

         $stmt->close();

         $stmt = null;



       }


    static public function eliminarCita($id){

      $stmt = Conexion::conectar()->prepare("DELETE FROM interview WHERE id = :id");

      $stmt-> bindParam(":id", $id, PDO::PARAM_STR);

      
      if ($stmt->execute()){

        return "ok";

      }else{

        return "false";
      }
      

      $stmt->close();

      $stmt = null;
    }
  }

  