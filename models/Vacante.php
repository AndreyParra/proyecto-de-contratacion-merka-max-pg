<?php

  require_once "Conexion.php";

  class Vacante extends Conexion {

  	public static function contarVacantes($tabla){

  		$stmt = Conexion::conectar()-> prepare("SELECT count(ID) as totalVacantes FROM $tabla"); 

  		$stmt -> execute();

  		return $stmt -> fetch();

  		$stmt->close();

  		$stmt = null;

  	}

    public static function Insertar($tabla, $Name, $Wage, $Description, $ID){

      $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (Name, Wage, Description, ID_Employee) 
        VALUES( :Name,:Wage,:Description,:ID)");


      $stmt-> bindParam(":Name", $Name, PDO::PARAM_STR);
      $stmt-> bindParam(":Wage", $Wage, PDO::PARAM_STR);
      $stmt-> bindParam(":Description", $Description, PDO::PARAM_STR);
      $stmt-> bindParam(":ID", $ID, PDO::PARAM_STR);

      if($stmt->execute()){
        
        return "true";
      
      } else{
        
        return "false";
      }
      
      $stmt->close();

      $stmt = null;
    } 

    public static function Select($tabla, $tabla1) {
        
      $stmt = Conexion::conectar()->prepare(
        "
        SELECT $tabla.ID, $tabla.Wage, $tabla.Description, $tabla1.Type
        FROM $tabla
        inner join $tabla1
        On $tabla.Name = $tabla1.ID;"
      );

      $stmt->execute();

      return $stmt->fetchAll();

      $stmt->close();

      $stmt = null;

      }

      public function InfoVacante($tabla, $item, $valor) {
     
      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

      $stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

      }

      static public function Update($tabla,$Name,$Wage,$Description,$ID,$icon,$IDVacant){

      $stmt = Conexion::conectar()->prepare("UPDATE $tabla set Wage = :Wage , description = :Description, ID_employee = :ID ,icon = :icon ,NameVacant = :Name where ID = IDVacant");


      $stmt-> bindParam(":Name", $Name, PDO::PARAM_STR);
      $stmt-> bindParam(":Wage", $Wage, PDO::PARAM_STR);
      $stmt-> bindParam(":Description", $Description, PDO::PARAM_STR);
      $stmt-> bindParam(":ID", $ID, PDO::PARAM_STR);
      $stmt-> bindParam(":icon", $icon, PDO::PARAM_STR);
      $stmt-> bindParam(":IDVacant", $IDVacant, PDO::PARAM_STR);

      if($stmt->execute()){
        
        return "true";
      
      } else{
        
        return "false";
      }
      
      $stmt->close();

      $stmt = null;
    } 
    public static function Roles($tabla) {
        
      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla Where ID <> 1");

      $stmt->execute();

      return $stmt->fetchAll();

      $stmt->close();

      $stmt = null;

      }
      public static function Datos($tabla) {
        
      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY ID");

      $stmt->execute();

      return $stmt->fetchAll();

      $stmt->close();

      $stmt = null;

      }

  }